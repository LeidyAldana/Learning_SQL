#!/bin/bash
variable1="Esto es una variable"
main () {
    echo "Hello World"
    echo $1
    echo $2
    if [ "$3" == "C" ] ; then
	    echo "$3" 
	    echo "^^"
    fi
}
variable1="Este es el cambio de la variable"
main "A" "B" C
echo $variable1
