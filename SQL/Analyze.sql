-- Analyze table

-- Determina cuantas filas hay en la tabla y como se asigna el almacenamiento. Ejemplo:

ANALYZE TABLE Alertas COMPUTE STATISTICS ;

-- También puede estimarse por cantidades. Ejemplo:

ANALYZE TABLE <table_name> ESTIMATE STATISTICS 50 PERCENT;

-- Eliminar las estadisticas asociadas a una tabla:

ANALYZE TABLE <table_name> DELETE STATISTICS;

-- json : https://blogs.oracle.com/jsondb/generating-json-data
