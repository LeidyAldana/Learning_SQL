-- Group by

-- Agrupa las filas que tienen el mismo valor, presente en ejemplos como encuentra el número de clientes en cada país. 

SELECT COUNT(idCliente), Pais
FROM Clientes
GROUP BY Pais;

-- Otro ejemplo:

SELECT COUNT(1), descripcion, idvalidacion FROM Alertas WHERE idproceso='1' 
GROUP BY descripcion, idvalidacion;
