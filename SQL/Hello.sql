-- Ejemplo de Bloque en Plsql, procedimiento:

--set serveroutput on size 10000
CREATE OR REPLACE PROCEDURE hola_mundo IS
--DECLARE 
        -- iniciaría la sección declarativa
        l_mensaje VARCHAR2(100) := '¡Hola Mundo!';
BEGIN    
	-- Sección ejecutable
        dbms_output.put_line(l_mensaje);
EXCEPTION
  WHEN OTHERS
    THEN
      DBMS_OUTPUT.put_line(SQLERRM);  -- SQLERRM es provista por Oracle	
END;
