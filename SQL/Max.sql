-- MAX

-- Retorna el valor más grande de la columna seleccionada

SELECT MAX(IDALERTA) FROM alertas; 

-- MIN

-- Retorna el valor más pequeño de la columna

SELECT MIN(IDALERTA) FROM alertas; 
