-- Uso de expresiones regulares para validar variables

set serveroutput on size 10000
BEGIN
if regexp_like('JUN 30' ,'(^(ENE|FEB|MAR|ABR|MAY|JUN|JUL|AGO|SEP|OCT|NOV|DIC) [[:digit:]]{2}$)') then
dbms_output.put_line('Si');
else
dbms_output.put_line('No');
end IF;
END;
/


delete from tabla1;
delete from tabla2;
delete from tabla3;
commit;

